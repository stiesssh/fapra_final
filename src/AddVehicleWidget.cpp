//
// Created by maumau on 03.09.19.
//

#include "AddVehicleWidget.h"
#include "ControlWidget.h"
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>

AddVehicleWidget::AddVehicleWidget() : QWidget() {

    lineEdit_name = new QLineEdit();
    lineEdit_area = new QLineEdit();
    lineEdit_mass = new QLineEdit();
    lineEdit_cw = new QLineEdit();
    lineEdit_tank = new QLineEdit();

    auto button_addVehicle = new QPushButton("Add Vehicle");

    auto layout_addVehicle = new QGridLayout;

    /* labels and lines to insert data */
    layout_addVehicle->addWidget(new QLabel("name :"), 0, 0);
    layout_addVehicle->addWidget(lineEdit_name, 0, 1);
    layout_addVehicle->addWidget(new QLabel("proj. area :"), 1, 0);
    layout_addVehicle->addWidget(lineEdit_area, 1, 1);
    layout_addVehicle->addWidget(new QLabel("mass :"), 2, 0);
    layout_addVehicle->addWidget(lineEdit_mass, 2, 1);
    layout_addVehicle->addWidget(new QLabel("cw :"), 3, 0);
    layout_addVehicle->addWidget(lineEdit_cw, 3, 1);
    layout_addVehicle->addWidget(new QLabel("tank :"), 4, 0);
    layout_addVehicle->addWidget(lineEdit_tank, 4, 1);
    layout_addVehicle->addWidget(button_addVehicle, 5, 0, 1, 2);

    this->setLayout(layout_addVehicle);

    /* connect */
    connect(button_addVehicle, SIGNAL(released()), this, SLOT(addVehicle()));
}

void AddVehicleWidget::addVehicle() {
    dynamic_cast<ControlWidget *>(this->parent())->setVehicle(new Vehicle(lineEdit_name->text(),
                                                                      lineEdit_area->text().toDouble(),
                                                                      lineEdit_mass->text().toDouble(),
                                                                      lineEdit_cw->text().toDouble(),
                                                                      lineEdit_tank->text().toDouble(), Vehicle::drivetype::OTTO));
    this->close();
}