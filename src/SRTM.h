//
// Created by maumau on 30.08.19.
//

#ifndef FAPRA_SRTM_H
#define FAPRA_SRTM_H

#include <string>

/**
 * Shuttle Radar Topography Mission
 */
class SRTM {

private:
    static const int ARC = 3600; //not 3601, because first and last cols/rows of successive files are duplicates.
    static const int TILES = 11;
    const int LATMIN = 46;
    const int LONMIN = 5;
    const int LATMAX = 56;
    const int LONMAX = 15;


    short data[TILES*ARC][TILES*ARC];

public:

    explicit  SRTM(std::string path = "../srtm")  {
        read(path);
    }

private:
    /**
     * read all srtm data required to cover germany. all data is saved in one big matrix.
     * the matrix looks like this:
     *
     *  |-----------|-----------|-----------|-------
     *  |  N56E005  |  N56E006  |  N55E007  |  ...
     *  |-----------|-----------|-----------|-------
     *  |  N55E005  |  N55E006  |  N55E007  |  ...
     *  |-----------|-----------|-----------|-------
     *  |    ...    |    ...    |    ...    |  ...
     *
     * @param path path to folder containing the srtm data
     */
    void read(std::string path);

public:
    /**
     * print quadrant at position (x,y). (0,0) is the top left corner (north sea).
     *
     * @param x position in direction of latitude
     * @param y position in direction of longitude
     * @param partial if true, print first and last 4 values of row or column only, otherwise print entire row or column
     */
    void print(int x, int y, bool partial = true);

    /**
     * print the entire data matrix
     */
    void print();

    /**
     * retrieves elevation at position (lat,lon) in meter.
     *
     * if (lat, lon) is none of the measured points the elevation is the weighted mean of the elevation of the four
     * surrounding data points.
     * the weight is the multiplicative inverse (kehrwert) of the distance between the given position and each
     * surrounding point.
     *
     * @param lat latitude
     * @param lon longitude
     * @return elevation at data [lat][lon] in meter
     */
    double getElevation(double lat, double lon);
};


#endif //FAPRA_SRTM_H
