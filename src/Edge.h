//
// Created by maumau on 15.05.19.
//

#ifndef OSMIUM_COUNT_EDGE_H
#define OSMIUM_COUNT_EDGE_H

/**
 * These edges are for motorised vehicles.
 */
class Edge {
public:
    unsigned int source; //ID in the nodes array
    unsigned int target; //ID in the nodes array
    double distance;
    double traveltime;
    double maxspeed;
    double ascent = 0; //the ankles sine

    Edge(unsigned int _source, unsigned int _target, double _distance, double _traveltime, double _maxspeed) :
            source(_source), target(_target), distance(_distance), traveltime(_traveltime), maxspeed(_maxspeed) {}

};


#endif //OSMIUM_COUNT_EDGE_H
