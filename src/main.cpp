//
// Created by maumau on 24.04.19.
//
#include <iostream>
#include <vector>
#include <set>
#include <osmium/io/file.hpp>
#include <osmium/io/any_input.hpp>
#include <osmium/visitor.hpp>

// Utility class gives us access to memory usage information
#include <osmium/util/memory.hpp>

#include <chrono> //time measurement

#include "WayHandler.cpp"
#include "NodeHandler.cpp"
#include "ControlWidget.h"
#include "Graph.h"
#include "SRTM.h"

#include <QtWidgets>
#include <memory>


int main(int argc, char *argv[]) {
    // this is for testing ui only. do not set to false!
    const bool DO_PARSE = true;

    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " osmfile [srtmfolder]\n";
        std::exit(1);
    }


    Graph *graph = new Graph();
    if (DO_PARSE) {
        try {
            auto start = std::chrono::high_resolution_clock::now();
            osmium::io::File input_file{argv[1]};

            { //get all osmiumways and make parsingEdges
                osmium::io::Reader reader{input_file, osmium::osm_entity_bits::way};
                WayHandler handler = WayHandler(graph, true);
                osmium::apply(reader, handler);
                reader.close();
            }

            { //get all osmiumnodes and make nodes
                SRTM *srtm;
                if (argc == 3)
                    srtm = new SRTM(argv[2]);
                else 
                    srtm = new SRTM();
                osmium::io::Reader reader{input_file, osmium::osm_entity_bits::node};
                NodeHandler handler = NodeHandler(graph, srtm);
                osmium::apply(reader, handler);
                reader.close();
                delete srtm;
            }


            { //get all osmiumways and make parsingEdges
                osmium::io::Reader reader{input_file, osmium::osm_entity_bits::way};
                WayHandler handler = WayHandler(graph);
                osmium::apply(reader, handler);
                reader.close();
            }
            //connect nodes and edges
            {
                auto initstart = std::chrono::high_resolution_clock::now();
                graph->init();
                auto initstop = std::chrono::high_resolution_clock::now();
                auto initduration = std::chrono::duration_cast<std::chrono::milliseconds>(initstop - initstart);
                std::cout << "initialised graph in " << initduration.count() << " milliseconds" << std::endl;
            }

            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
            std::cout << "parsed and initialised everything in " << duration.count() << " seconds" << std::endl;

            graph->print();

            osmium::MemoryUsage memory;

            std::cout << "\nMemory used: " << memory.peak() << " MBytes\n";

        } catch (const std::exception &e) {
            // All exceptions used by the Osmium library derive from std::exception.
            std::cerr << e.what() << '\n';
            std::exit(1);
        }
    }

    QApplication app(argc, argv);


    //doing gui stuff
    auto window = new ControlWidget(graph);
    window->resize(1340, 940);
    window->show();

    return app.exec();
}
