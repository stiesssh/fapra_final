//
// Created by maumau on 05.09.19.
//

#ifndef FAPRA_PREDEFINEDVEHICLESWIDGET_H
#define FAPRA_PREDEFINEDVEHICLESWIDGET_H

#include <iostream>

#include <vector>

#include <QListWidget>
#include <QLineEdit>
#include "Vehicle.h"


class PredefinedVehiclesWidget : public QListWidget {
Q_OBJECT

public:
    PredefinedVehiclesWidget();
private:
    // car [ are, mass , cw , tank]
    // 1040 kg,  cw = 0,31 area = 2,07 tank = 41 l
    std::vector<Vehicle *> predefs;

public slots:
    void setVehicle(int r);

};


#endif //FAPRA_PREDEFINEDVEHICLESWIDGET_H
