//
// Created by maumau on 03.09.19.
//

#ifndef FAPRA_ADDVEHICLEWIDGET_H
#define FAPRA_ADDVEHICLEWIDGET_H

#include <QtWidgets/QWidget>
#include <QLineEdit>


class AddVehicleWidget : public QWidget  {
Q_OBJECT

public :
AddVehicleWidget();

private :
    QLineEdit *lineEdit_area;
    QLineEdit *lineEdit_name;
    QLineEdit *lineEdit_mass;
    QLineEdit *lineEdit_cw;
    QLineEdit *lineEdit_tank;

private slots:

    void addVehicle();

};


#endif //FAPRA_ADDVEHICLEWIDGET_H
