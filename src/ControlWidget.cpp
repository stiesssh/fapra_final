//
// Created by maumau on 23.05.19.
//

#include "ControlWidget.h"

#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include <QWebEngineView>
#include <QWebChannel>
#include <QtWidgets/QWidget>

ControlWidget::ControlWidget(Graph * _graph) : QWidget(), graph(_graph) {
    // ???
    setAttribute(Qt::WA_DeleteOnClose, true);

    //load Map
    view = new QWebEngineView();
    view->load(QUrl("qrc:/html/map.html"));

    //connect with webchannel
    auto channel = new QWebChannel(this);
    view->page()->setWebChannel(channel);
    channel->registerObject(QStringLiteral("webobj"), this);


    /*labels and line edits */
    label_vname = new QLabel();
    label_va = new QLabel();
    label_vm = new QLabel();
    label_vcw = new QLabel();
    label_vtank = new QLabel();
    label_vtank_unit = new QLabel;
    label_vtank_unit->setText("l");
    lineEdit_luggage = new QLineEdit();
    lineEdit_luggage->setReadOnly(true);

    label_dijkstraTime = new QLabel("-");
    label_totalTime = new QLabel("-");
    label_totalDistance = new QLabel("-");
    label_totalWork = new QLabel("-");

    /* Buttons */
    auto button_s = new QPushButton("Choose start on map");
    auto button_d = new QPushButton("Choose dest on map");
    auto button = new QPushButton("Route me");
    auto button_makeCar = new QPushButton("Make a new Vehicle");
    auto button_chooseCar = new QPushButton("Choose a Vehicle");
    auto button_updateGas = new QPushButton("Updated Gas Display");

    /* Mode of Routing */
    auto routing_mode = new QGroupBox("Routing Options : ");
    auto radioRouting1 = new QRadioButton("shortest route");
    radioRouting1->toggle();
    auto radioRouting2 = new QRadioButton("fastest route");
    auto radioRouting3 = new QRadioButton("most economical route");

    {
        auto *vbox = new QVBoxLayout;
        vbox->addWidget(radioRouting1);
        vbox->addWidget(radioRouting2);
        vbox->addWidget(radioRouting3);
        routing_mode->setLayout(vbox);
    }

    /* Gas Station options */
    auto gas_mode = new QGroupBox("Gas Station Options : ");
    auto radioGas1 = new QRadioButton("display recommended gas stations");
    auto radioGas2 = new QRadioButton("display all gas stations on route");
    auto radioGas3 = new QRadioButton("display all gas stations");
    auto radioGas4 = new QRadioButton("display no gas stations");
    radioGas4->toggle();

    {
        auto *vbox = new QVBoxLayout;
        vbox->addWidget(radioGas1);
        vbox->addWidget(radioGas2);
        vbox->addWidget(radioGas3);
        vbox->addWidget(radioGas4);
        gas_mode->setLayout(vbox);
    }

    /* Displays */
    auto vehicleDisplay = new QGridLayout();
    vehicleDisplay->addWidget(new QLabel("Current Vehicle: "), 1, 0);
    vehicleDisplay->addWidget(label_vname, 1, 1, 1, 2);
    vehicleDisplay->addWidget(new QLabel("Area: "), 2, 0);
    vehicleDisplay->addWidget(label_va, 2, 1);
    vehicleDisplay->addWidget(new QLabel("m^2"), 2, 2);

    vehicleDisplay->addWidget(new QLabel("Mass: "), 3, 0);
    vehicleDisplay->addWidget(label_vm, 3, 1);
    vehicleDisplay->addWidget(new QLabel("kg"), 3, 2);

    vehicleDisplay->addWidget(new QLabel("cw: "), 4, 0);
    vehicleDisplay->addWidget(label_vcw, 4, 1);
    vehicleDisplay->addWidget(new QLabel("-"), 4, 2);

    vehicleDisplay->addWidget(new QLabel("Tank: "), 5, 0);
    vehicleDisplay->addWidget(label_vtank, 5, 1);
    vehicleDisplay->addWidget(label_vtank_unit, 5, 2);

    vehicleDisplay->addWidget(new QLabel("Luggage: "), 6, 0);
    vehicleDisplay->addWidget(lineEdit_luggage, 6, 1);
    vehicleDisplay->addWidget(new QLabel("kg"), 6, 2);


    auto resultDisplay = new QGridLayout();
    resultDisplay->addWidget(new QLabel("Dijkstra: "), 1, 0);
    resultDisplay->addWidget(label_dijkstraTime, 1, 1);
    resultDisplay->addWidget(new QLabel("ms"), 1, 2);

    resultDisplay->addWidget(new QLabel("Distance: "), 2, 0);
    resultDisplay->addWidget(label_totalDistance, 2, 1);
    resultDisplay->addWidget(new QLabel("km"), 2, 2);

    resultDisplay->addWidget(new QLabel("Traveltime: "), 3, 0);
    resultDisplay->addWidget(label_totalTime, 3, 1);
    resultDisplay->addWidget(new QLabel("h"), 3, 2);

    resultDisplay->addWidget(new QLabel("Work: "), 4, 0);
    resultDisplay->addWidget(label_totalWork, 4, 1);
    resultDisplay->addWidget(new QLabel("kWh"), 4, 2);


    auto placeholder = new QWidget();
    auto p1 = new QVBoxLayout();
    p1->addStretch(1);
    placeholder->setLayout(p1);


    /* the top most layout */
    auto layout = new QGridLayout;

    /* buttons to choose start and destination*/
    layout->addWidget(button_s, 0, 0, 1, 2);
    layout->addWidget(button_d, 1, 0, 1, 2);

    /* button to start route calculation */
    layout->addWidget(button, 2, 0, 1, 2);

    /* checkbox for how to route*/
    layout->addWidget(routing_mode, 3, 0, 1, 2);

    /* vehicle display */
    layout->addLayout(vehicleDisplay, 4, 0, 1, 2);

    /* label for beautificaltion */
    layout->addWidget(new QLabel("Vehicle Options : "), 5, 0, 1, 2);

    /*button to choose a predefined car*/
    layout->addWidget(button_chooseCar, 6, 0, 1, 2);

    /*button to make a new car*/
    layout->addWidget(button_makeCar, 7, 0, 1, 2);

    /*gas options*/
    layout->addWidget(gas_mode, 8, 0, 1, 2);
    layout->addWidget(button_updateGas, 9, 0, 1, 2);

    /* result display */
    layout->addLayout(resultDisplay, 10, 0, 1, 2);

    /*placeholder*/
    layout->addWidget(placeholder, 15, 0, 1, 2);


    /* connect buttons */
    connect(button, SIGNAL (released()), this, SLOT (routeme()));
    connect(button_updateGas, SIGNAL (released()), this, SLOT (updateGas()));

    connect(button_makeCar, SIGNAL (released()), this, SLOT (makeVehicle()));
    connect(button_chooseCar, SIGNAL (released()), this, SLOT (chooseVehicle()));

    /* connect radiobuttons */
    connect(radioRouting1, SIGNAL (toggled(bool)), this, SLOT (shortestToggled(bool)));
    connect(radioRouting2, SIGNAL (toggled(bool)), this, SLOT (fastestToggled(bool)));
    connect(radioRouting3, SIGNAL (toggled(bool)), this, SLOT (economicalToggled(bool)));
    connect(radioGas1, SIGNAL (toggled(bool)), this, SLOT (recommendedToggled(bool)));
    connect(radioGas2, SIGNAL (toggled(bool)), this, SLOT (allOnRouteToggled(bool)));
    connect(radioGas3, SIGNAL (toggled(bool)), this, SLOT (allAllToggled(bool)));
    connect(radioGas4, SIGNAL (toggled(bool)), this, SLOT (noneToggled(bool)));

    /* connect choose buttons */
    connect(button_s, SIGNAL(released()), this, SLOT(choosingS()));
    connect(button_d, SIGNAL(released()), this, SLOT(choosingD()));

    connect(lineEdit_luggage, SIGNAL(textChanged(const QString)), this, SLOT(updateLuggageMass(const QString)));

    //ugly hierarchy but it works
    auto widgetwidget = new QWidget();
    widgetwidget->setLayout(layout);
    widgetwidget->setMaximumWidth(300);

    auto hlayout = new QHBoxLayout();

    hlayout->addWidget(widgetwidget);
    hlayout->addWidget(view);

    this->setLayout(hlayout);

    predef = new PredefinedVehiclesWidget();
    predef->setParent(this, Qt::Dialog);
    predef->setVehicle(0);
}


void ControlWidget::routeme() {
    std::cout << "clicked route me " << std::endl;

    if (0.0 == slon || 0.0 == slat) {
        QString code = QStringLiteral("alert( 'Please choose start!' );");
        view->page()->runJavaScript(code);
        return;
    }
    if (0.0 == dlon || 0.0 == dlat) {
        QString code = QStringLiteral("alert( 'Please choose destination!' );");
        view->page()->runJavaScript(code);
        return;
    }

    unsigned int start = graph->nearestNeighbour(slat, slon);
    unsigned int dest = graph->nearestNeighbour(dlat, dlon);

    {
        std::cout << "start dijkstra" << std::endl;
        auto t0 = std::chrono::high_resolution_clock::now();
        graph->dijkstra(start, dest, type, anythingToggld);
        auto t1 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        label_dijkstraTime->setText(QString::number(duration.count()));
        std::cout << "dijkstra finished in " << duration.count() << " milliseconds" << std::endl;
    }
    paint();
    //std::cout << "painted route " << std::endl;

}

void ControlWidget::getlatlon(const QString &jslat, const QString &jslng){
    Node n  = graph->nodes[graph->nearestNeighbour(jslat.toDouble(), jslng.toDouble())];

    if (choosing == selectionMode::dest) {
        view->page()->runJavaScript(QStringLiteral("destMarker.remove()"));
        //set lat and lon
        dlon = n.lon;
        dlat = n.lat;

        //make marker
        QString code = QStringLiteral("destMarker = L.marker([");
        code.append(QString::number(dlat)).append(QStringLiteral(", ")).append(QString::number(dlon)).append(QStringLiteral("]); destMarker.addTo(mymap);"));
        //make popup
        code.append(QStringLiteral("destPopup.setContent('<p>Destination  <br />Lat: "));
        code.append(QString::number(dlat)).append(QStringLiteral("<br />Lon: "));
        code.append(QString::number(dlon)).append(QStringLiteral("</p>');"));

        code.append(QStringLiteral("destMarker.bindPopup(destPopup).openPopup();"));

        view->page()->runJavaScript(code);


    } else if (choosing == selectionMode::start) {
        view->page()->runJavaScript(QStringLiteral("startMarker.remove()"));
        //set lat and lon
        slon = n.lon;
        slat = n.lat;
        //make marker
        QString code = QStringLiteral("startMarker = L.marker([");
        code.append(QString::number(slat)).append(QStringLiteral(", ")).append(QString::number(slon)).append(QStringLiteral("]); startMarker.addTo(mymap);"));
        //make popup
        code.append(QStringLiteral("startPopup.setContent('<p>Start <br />Lat: "));
        code.append(QString::number(slat)).append(QStringLiteral("<br />Lon: "));
        code.append(QString::number(slon)).append(QStringLiteral("</p>');"));

        code.append(QStringLiteral("startMarker.bindPopup(startPopup).openPopup();"));

        view->page()->runJavaScript(code);
    }
    // reset choosing mode
    choosing = selectionMode::none;
}

void ControlWidget::updateGas () {
    //remove all gas stations from display
    view->page()->runJavaScript(QStringLiteral("for (var i=0; i<fuelmarkers.length; i++) { fuelmarkers[i].remove(); } fuelmarkers = [];"));

    switch (gasDisplayMode) {
        case gasDisplayMode::none :
            return;
        case gasDisplayMode::recommended :
            displayGas();
            updateResultDisplay(); //because luggage might have changed
            break;
        case gasDisplayMode::allonroute :
            displayGas();
            break;
        case gasDisplayMode::allall :
            displayAllGas();
            QString code = QStringLiteral("alert( 'Be careful, map might now react with huge delay!' );");
            view->page()->runJavaScript(code);
            break;
    }
}

void ControlWidget::displayGas () {
    graph->vehicle->resetWork();

    std::vector<unsigned int> path = graph->getPath();

    QString code;
    if (graph->vehicle->type == Vehicle::drivetype::ELEKTRO)
        code = QStringLiteral("myIcon = orangeIcon;");
    else
        code = QStringLiteral("myIcon = blueIcon;");
    view->page()->runJavaScript(code);

    for(unsigned int edgeindex : path) {
        /*marker = L.marker([..]) fuelmarkers.push(marker); }*/

        // reduce gas
        Edge e = graph->motEdges.at(edgeindex);
        double work = e.traveltime * graph->vehicle->clacPges(e.maxspeed, e.ascent);
        graph->vehicle->doWork(work);
        if (gasDisplayMode::allonroute == gasDisplayMode || graph->vehicle->isEmpty()) {
            graph->vehicle->resetWork();
            Node node = graph->nearestGasstation(graph->nodes[e.source].lat, graph->nodes[e.source].lon);
            QString codex = QStringLiteral("var marker = L.marker([");
            codex.append(QString::number(node.lat)).append(QStringLiteral(", ")).append(QString::number(node.lon)).append(
                    QStringLiteral("], {icon: myIcon}).addTo(mymap); fuelmarkers.push(marker);"));
            view->page()->runJavaScript(codex);
        }
    }
}

void ControlWidget::displayAllGas () {
    for (auto station : graph->fuel) {
        QString codex = QStringLiteral("var marker = L.marker([");
        codex.append(QString::number(station.lat)).append(QStringLiteral(", ")).append(QString::number(station.lon)).append(
                QStringLiteral("], {icon: blueIcon}).addTo(mymap); fuelmarkers.push(marker);"));
        view->page()->runJavaScript(codex);
    }
    for (auto station : graph->charging) {
        QString codex = QStringLiteral("var marker = L.marker([");
        codex.append(QString::number(station.lat)).append(QStringLiteral(", ")).append(QString::number(station.lon)).append(
                QStringLiteral("], {icon: orangeIcon}).addTo(mymap); fuelmarkers.push(marker);"));
        view->page()->runJavaScript(codex);
    }
}

void ControlWidget::updateResultDisplay () {

    //calculate totals
    double totalWork = 0.0;
    double totalTime = 0.0;
    double totalDistance = 0.0;

    // construct new polyline
    std::vector<unsigned int> path = graph->getPath();

    for(unsigned int edgeindex : path) {
        Edge e = graph->motEdges.at(edgeindex);
        totalDistance += e.distance;
        totalTime += e.traveltime;
        totalWork += e.traveltime * graph->vehicle->clacPges(e.maxspeed, e.ascent);
    }

    label_totalWork->setText(QString::number(totalWork/(3600.0*1000.0)));
    label_totalTime->setText(QString::number(totalTime/3600.0));
    label_totalDistance->setText(QString::number(totalDistance/1000.0));
}

void ControlWidget::resetRoute () {
    //remove painted route
    view->page()->runJavaScript(QStringLiteral("polyline.remove();"));
    //remove gas stations on display, if they depend on the calculated route
    if (gasDisplayMode::allonroute == gasDisplayMode || gasDisplayMode::recommended == gasDisplayMode)
        view->page()->runJavaScript(QStringLiteral("for (var i=0; i<fuelmarkers.length; i++) { fuelmarkers[i].remove(); } fuelmarkers = [];"));
}