//
// Created by maumau on 04.09.19.
//

#ifndef FAPRA_UTILITY_H
#define FAPRA_UTILITY_H

#include <cmath> //for pi, sin, cos, hypot

class Utility {
public:
  /**
   * calculates the distance between two points in meters
   *
   * @param lat1 latitude of the first point
   * @param lon1 longitude of the first point
   * @param lat2 latitude of the second point
   * @param lon2 longitude of the second point
   * @return distance in meters
   */
    static double distance(double lat1, double lon1, double lat2, double lon2) {
        const double earth = 6371009; // radius of earth in meter
        const double pi = acos(-1);
        //calculate radians
        double rlat1 = lat1 * (pi / 180.0);
        double rlat2 = lat2 * (pi / 180.0);
        //calculate radian distances
        double rdlat = (lat1 - lat2) * (pi / 180.0);
        double rdlon = (lon1 - lon2) * (pi / 180.0);

        //sign must not be consider because the sinuses are quadrated and the cosines are symmetric to the axis.
        double a = std::sin(rdlat / 2.0) * std::sin(rdlat / 2.0) +
                   std::cos(rlat1) * std::cos(rlat2) * std::sin(rdlon / 2.0) * std::sin(rdlon / 2.0);

        double c = 2.0 * std::atan2(std::sqrt(a), std::sqrt(1 - a));

        return earth * c;
    }
};

#endif //FAPRA_UTILITY_H