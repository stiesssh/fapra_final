//
// Created by maumau on 15.05.19.
//

#ifndef OSMIUM_COUNT_DIJKSTRANODE_H
#define OSMIUM_COUNT_DIJKSTRANODE_H


class DijkstraNode {
public:
    unsigned int id;
    double cost;

    DijkstraNode(unsigned int _id, double _cost) : id(_id), cost(_cost) {}
    DijkstraNode(){}

    bool operator<(const DijkstraNode& n) const {
        return n.cost < this->cost;
    }
};


#endif //OSMIUM_COUNT_DIJKSTRANODE_H
