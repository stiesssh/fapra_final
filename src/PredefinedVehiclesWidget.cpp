//
// Created by maumau on 05.09.19.
//

#include "PredefinedVehiclesWidget.h"
#include "ControlWidget.h"

PredefinedVehiclesWidget::PredefinedVehiclesWidget() : QListWidget() {
    predefs.emplace_back(new Vehicle("Citroen C2", 2.07, 1040.0, 0.31, 5.0, Vehicle::drivetype::OTTO)); //41l!!
    predefs.emplace_back(new Vehicle("Opel Adam", 2.12, 1178.0, 0.32, 35.0, Vehicle::drivetype::OTTO)); //1086–1178 kg
    predefs.emplace_back(new Vehicle("VW GOLF VII", 2.19, 1615.0, 0.27, 50.0, Vehicle::drivetype::OTTO)); //1205–1615 kg
    predefs.emplace_back(new Vehicle("SUZUKI GSX-R", 0.76, 176.0, 0.581, 19.0, Vehicle::drivetype::OTTO)); //176kg
    predefs.emplace_back(new Vehicle("Smart Fortwo ", 2.25, 1010, 0.38, 28.0, Vehicle::drivetype::OTTO)); //1010 kg, 28l/5l reserve
    predefs.emplace_back(new Vehicle("Smart Fortwo (Diesel)", 2.25, 1010, 0.38, 28.0, Vehicle::drivetype::DIESEL)); //1010 kg, 17.6 Kwh
    predefs.emplace_back(new Vehicle("Smart Fortwo E", 2.25, 1010, 0.38, 17.6, Vehicle::drivetype::ELEKTRO)); //1010 kg, 17.6 Kwh

    //880–1040 kg


    new QListWidgetItem(tr("Citroen C2"), this);
    new QListWidgetItem(tr("Opel Adam"), this);
    new QListWidgetItem(tr("VW GOLF VII"), this);
    new QListWidgetItem(tr("SUZUKI GSX-R"), this);
    new QListWidgetItem(tr("Smart Fortwo"), this);
    new QListWidgetItem(tr("Smart Fortwo (Diesel)"), this);
    new QListWidgetItem(tr("Smart Fortwo E"), this);

    connect(this, SIGNAL (currentRowChanged(int)), this, SLOT (setVehicle(int)));
}

void PredefinedVehiclesWidget::setVehicle(int r) {
    std::cout << "row :" << r <<std::endl;
    dynamic_cast<ControlWidget *>(this->parent())->setVehicle(predefs.at(r));
    this->hide();
}