//
// Created by maumau on 08.05.19.
//

#include <osmium/handler.hpp>
#include "SRTM.h"
#include "Graph.h"
#include "Node.h"
#include <osmium/osm/node.hpp>
#include <osmium/osm/area.hpp>

class NodeHandler : public osmium::handler::Handler {
private:
    Graph* graph;
    SRTM* srtm;
public:
    NodeHandler(Graph* _graph, SRTM* _srtm) : osmium::handler::Handler(), graph(_graph), srtm(_srtm) {}

    // This callback is called by osmium::apply for each node in the data.
    void node(osmium::Node &node) noexcept{
        //germany has about 60 000 000 nodes. thus this fits into an int

        unsigned int nodeId = graph->nodes.size();
        unsigned long osmiumId = node.positive_id();

        //fuel
        const char *amenity = node.tags()["amenity"];
        const char *car = node.tags()["car"];
        const char *access = node.tags()["access"];

        if (nullptr != amenity && (nullptr == access || (strcmp("private", access) != 0 && strcmp("no", access) != 0 && strcmp("customers", access) != 0))) {
            if (strcmp("charging_station", amenity) == 0 && (nullptr == car || strcmp("yes", car) == 0)){
                graph->charging.emplace_back(Node(node.location().lat(), node.location().lon()));
            }

            if (strcmp("fuel", amenity) == 0) {
                graph->fuel.emplace_back(Node(node.location().lat(), node.location().lon()));
            }
        }

        if (graph->knownNodes.find(osmiumId) == graph->knownNodes.end()) {
            //node not relevant
            return;
        }

        auto lat = node.location().lat();
        auto lon = node.location().lon();

        //add new node, init offset
        graph->nodes.emplace_back(Node(lat, lon));
        //add elevation.
        graph->elevation.emplace_back(srtm->getElevation(lat, lon));
        //add id mapping
        graph->osmiumToNode.emplace(osmiumId, nodeId);

        //remove from knownNodes
        graph->knownNodes.erase(node.positive_id());
    }
};
