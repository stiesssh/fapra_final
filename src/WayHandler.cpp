//
// Created by maumau on 08.05.19.
//

#include <osmium/handler.hpp>
#include <osmium/osm/way.hpp>

#include "Graph.h"

class WayHandler : public osmium::handler::Handler {
private:
    Graph *graph;

    bool preprocess;

    struct cstrless {
        bool operator()(const char *a, const char *b) {
            return strcmp(a, b) < 0;
        }
    };


//unclassified : minor public roads typically at the lowest level of the interconnecting grid network

    std::set<const char *, cstrless> car = {"motorway", "primary", "secondary", "tertiary", "trunk", "motorway_link",
                                            "primary_link", "secondary_link", "tertiary_link", "trunk_link",
                                            "unclassified",
                                            "residential", "living_street", "service"};
/*
 * corridor : some indoor hallway
 * services : services station. not a service road.
 */
// values of highway tag that are not mentioned in the wiki
    std::set<const char *, cstrless> ignoreHighway = {"construction", "platform",
                                                      "bus_stop", "rest_area", "road", "proposed", "planned",
                                                      "abandoned", "emergency", "disused", "virtual", "via_ferrata",
                                                      "demolished", "private_footway", "yes", "no", "none", "razed",
                                                      "dismantled",
                                                      "access", "bus_guideway", "historic", "stop_line",
                                                      "loading_place",
                                                      "street_lamp", "parkingaisle", "ramp", "trail", "informal_path",
                                                      "escape", "access_ramp", "emergency_bay",
                                                      "emergency_access_point",
                                                      "turning_circle", "driveway", "passing_place",
                                                      "raceway", "corridor", "traffic_island", "services", "layby",
                                                      "residential", "living_street", "footway", "footpath", "foot",
                                                      "pedestrian",
                                                      "path", "steps",
                                                      "elevator", "crossing", "service", "track",
                                                      "bridleway", "cycleway"};
// values of service tag not mentioned in the wiki
    std::set<const char *, cstrless> ignoreService = {
            "parking_aisle", // Fahrgassen auf einem Parkplatz (nicht für Zufahrten oder Verbindungswege zwischen Fahrgassen)
            "parkting_aisle", // because people are to stupid to write -.-
            "drive-through", // Durchfahrtstraßen von Schnellrestaurants
            "emergency_access", // eine Feuerwehrzufahrt
            "parking", "bus", "parking_entrance", "delivery", "destination",
            "bus_lane", "busway", "weigh_station", "car_wash", "yard",
            "layby", "driveyard", "dirveway", "emergency", "fuel", "spur", "parking_space", "psv", "PSV", "access",
            "nutrition", "agricultural", "taxi",
            "drive_through", "Zufahrt", "residential", "residental", "slipway", "busbay", "fine_gravel", "gravel",
            "grass", "fire_lane", "Betriebszufahrt", "Campingplatzweg", "siding"};

    std::set<const char *, cstrless> accessibleService = {
            "driveway",  // A driveway is a service road leading to a residence or busines
            "alley"}; // An alley is a service road usually located between properties for access to utilities

// values of access tag that are not accessible for public
    std::set<const char *, cstrless> prohibited = {"no", "private", "discouraged"};

    //standart speed in km/h
    const short highspeed = 130;
    const short ruralspeed = 100;
    const short urbanspeed = 50;
    const short zonespeed = 30;
    const short walkspeed = 5;


public:
    explicit WayHandler(Graph *_graph, bool _preprocess = false) : osmium::handler::Handler(), graph(_graph), preprocess(_preprocess) {}

    // This callback is called by osmium::apply for each way in the data.
    void way(osmium::Way &way) noexcept {
        //get tag value
        const char *highway = way.tags()["highway"];
        const char *access = way.tags()["access"];
        const char *oneway = way.tags()["oneway"];
        const char *service = way.tags()["service"];
        const char *maxspeed = way.tags()["maxspeed"];

        // no highway at all
        if (nullptr == highway) {
            return;
        }
        // access is prohibited
        if (nullptr != access && prohibited.find(access) != prohibited.end()) {
            return;
        }
        if (nullptr != service && accessibleService.find(service) == accessibleService.end()) {
            return;
        }

        if (preprocess && car.find(highway) != car.end()){
            for (unsigned int i = 0; i < way.nodes().size(); i++) {
                graph->knownNodes.insert(way.nodes()[i].positive_ref());
            }
            return;
        }


        //a street for motorised vehicles
        if (car.find(highway) != car.end()) {
            short maxSpeed = 0;
            if (nullptr == maxspeed) {
                maxSpeed = speedOnWay(highway);
            } else {
                maxSpeed = determineMaxSpeed(maxspeed);
            }

            unsigned long sourceId = way.nodes()[0].positive_ref();
            unsigned long targetId = 0;

            //graph->knownNodes.insert(sourceId);

            //in case i question my self later on:
            //the code block is repeated three times, cause i do not want to check the oneway-ness every iteration
            if (isOneway(oneway) == 1) { //one way
                for (unsigned int i = 1; i < way.nodes().size(); i++) {
                    targetId = way.nodes()[i].positive_ref();
                    //graph->knownNodes.insert(targetId);
                    //forward edge only
                    graph->motorwayParsingEdges.emplace_back(ParsingEdge(sourceId, targetId, maxSpeed));

                    sourceId = targetId;
                }
            } else if (isOneway(oneway) == 0) { //two way
                for (unsigned int i = 1; i < way.nodes().size(); i++) {
                    targetId = way.nodes()[i].positive_ref();
                    //graph->knownNodes.insert(targetId);
                    //forward edge
                    graph->motorwayParsingEdges.emplace_back(ParsingEdge(sourceId, targetId, maxSpeed));
                    //backward edge
                    graph->motorwayParsingEdges.emplace_back(ParsingEdge(targetId, sourceId, maxSpeed));

                    sourceId = targetId;
                }
            } else if (isOneway(oneway) == -1) { //inverse
                for (unsigned int i = 1; i < way.nodes().size(); i++) {
                    targetId = way.nodes()[i].positive_ref();
                    //graph->knownNodes.insert(targetId);
                    //backward edge only
                    graph->motorwayParsingEdges.emplace_back(ParsingEdge(targetId, sourceId, maxSpeed));

                    sourceId = targetId;
                }
            }
        }
        if (ignoreHighway.find(highway) == ignoreHighway.end() && car.find(highway) == car.end()) {
            std::cout << "unidentified highway : " << highway << std::endl;
        }
    }

    /**
     * parse max speed tag
     *
     * @param maxSpeed value of tag maxspeed
     * @return maxspeed in m/s
     */
    short determineMaxSpeed(const char *maxSpeed) {
        //parseable and reasonable number
        if (0 < atoi(maxSpeed) && 150 > atoi(maxSpeed))
            return atoi(maxSpeed);
        //within the city
        if ((strcmp(maxSpeed, "DE:urban") == 0) | (strcmp(maxSpeed, "AT:urban") == 0))
            return urbanspeed;
        //pedestrians speed
        if ((strcmp(maxSpeed, "walk") == 0) | (strcmp(maxSpeed, "DE:living_street") == 0) |
            (strcmp(maxSpeed, "Schrittgeschwindigkeit") == 0) | (strcmp(maxSpeed, "DE:walk") == 0))
            return walkspeed;
        //not in the city
        if ((strcmp(maxSpeed, "DE:rural") == 0) | (strcmp(maxSpeed, "AT:rural") == 0))
            return ruralspeed;
        //german highway (autobahn)
        if ((strcmp(maxSpeed, "signals") == 0) | (strcmp(maxSpeed, "none") == 0))
            return highspeed;
        //30er zone
        if ((strcmp(maxSpeed, "DE:zone:30") == 0) | (strcmp(maxSpeed, "DE:zone30") == 0))
            return zonespeed;

        //report weird things
        std::cout << "unidentified speed : " << maxSpeed << std::endl;
        //return default value
        return urbanspeed;
    }

    short speedOnWay(const char *way) {
        if ((strcmp(way, "motorway") == 0) | (strcmp(way, "trunk") == 0))
            return 130;
        if ((strcmp(way, "primary") == 0) | (strcmp(way, "secondary") == 0) | (strcmp(way, "tertiary") == 0))
            return 100;
        if ((strcmp(way, "motorway_link") == 0) | (strcmp(way, "trunk_link") == 0) |
            (strcmp(way, "primary_link") == 0) | (strcmp(way, "secondary_link") == 0) |
            (strcmp(way, "tertiary_link") == 0))
            return 70;
        if ((strcmp(way, "unclassified") == 0) | (strcmp(way, "service") == 0))
            return urbanspeed;
        if (strcmp(way, "residential") == 0)
            return zonespeed;
        if (strcmp(way, "living_street") == 0)
            return walkspeed;
        return
            ruralspeed;
    }


    /**
     * determines whether a way is oneway or not.
     * defaults to twoways if oneway tag is not specified, as this is the case for most smaller streets and most smaller streets are accessible in both directions.
     * defaults to oneway, if the tag is specified, but not parseable.
     *
     * @param oneway value of tag oneway
     * @return 0 if it is twoways, 1 if it is oneway, -1 if it is inverse
     */
    int isOneway(const char *oneway) {
        if (nullptr == oneway)
            return 0;
        if (strcmp("no", oneway) == 0 || strcmp("0", oneway) == 0 || strcmp("false", oneway) == 0)
            return 0;
        //inverse street
        if (strcmp("-1", oneway) == 0)
            return -1;
        return 1;
    }
};

