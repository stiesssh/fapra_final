//
// Created by maumau on 08.05.19.
//

#ifndef OSMIUM_COUNT_GRAPH_H
#define OSMIUM_COUNT_GRAPH_H

#include <cassert>

#include <queue>
#include <vector>
#include <set>
#include <unordered_map>
#include <algorithm> //for sort
#include <iostream>
#include <cmath> //for pi, sin, cos, hypot
#include <limits>
#include <stdexcept>


#include "Node.h"
#include "ParsingEdge.h"
#include "Edge.h"
#include "DijkstraNode.h"
#include "SRTM.h"
#include "Vehicle.h"

class Graph {
public:
    // handy constants
    const long maxlong = std::numeric_limits<long>::max();
    const unsigned int maxuint = std::numeric_limits<unsigned int>::max();

    // the nodes
    std::vector<Node> nodes = std::vector<Node>();

private:
    //the heap and other data structures for dijkstra
    std::priority_queue<DijkstraNode> heap = std::priority_queue<DijkstraNode>();
    std::vector<unsigned int> precedingEdge = std::vector<unsigned int>();
    std::vector<double> currentmin = std::vector<double>();

public:
    //edges for motorised vehicles
    std::vector<ParsingEdge> motorwayParsingEdges = std::vector<ParsingEdge>(); //deleted after parsing
    std::vector<Edge> motEdges = std::vector<Edge>();
    std::vector<unsigned int> motorwayOffsets = std::vector<unsigned int>();

    //helper data structures during parsing
    std::set<unsigned long> knownNodes = std::set<unsigned long>();
    std::vector<double> elevation = std::vector<double>();
    std::unordered_map<unsigned long, unsigned int> osmiumToNode = std::unordered_map<unsigned long, unsigned int>();

    //gas stations
    std::vector<Node> fuel = std::vector<Node>();
    std::vector<Node> charging = std::vector<Node>();

public:
    //to remember the last executed dijkstra
    unsigned int currentStartId = 0;
    unsigned int currentDestId = 0;
    std::vector<unsigned int> path = std::vector<unsigned int>();


    //vehicles
    Vehicle* vehicle;

    explicit Graph() {};

    void print() {
        std::cout << "nodes : " << nodes.size() << std::endl;
        std::cout << "motorway edges : " << motEdges.size() << std::endl;
    }

    void init();

    void setVehicle(Vehicle* v) {
        vehicle = v;
    }

private:
    void connectMotorways();

    /**
     * calculates for each edge sin(a). a is the ankle of a-/descension of that edge, wrt to its source node.
     *
     * the calculation takes source and target node as the endpoints of the hypotenuse of a right-angled triangle.
     *
     *          t
     *       .: |
     *    .:    |
     *  s ------|
     *
     *  it takes the difference in elevation as the opposite side (gegenkathete) to s and the distance between s and t
     *  as the adjacent side (ankathete) to s.
     *
     */
    void setAscension();

public:
    /**
     * calculates the nearest node to the given point and returns the nodes index in the node array.
     *
     * @param lat the point's latitude
     * @param lon the point's longitude
     * @return the nearest nodes index in the nodes vector.
     */
    size_t nearestNeighbour(double lat, double lon);

    /**
     * calculates the nearest gas station to the given point.
     *
     * @param lat the point's latitude
     * @param lon the point's longitude
     * @return the nearest gas station
     */
    Node nearestGasstation(double lat, double lon);

    /**
     * calculates the path from current start to destination node.
     *
     * @return the path from start to destination. the path is empty, if start and destination are not connected.
     */
    std::vector<unsigned int> &getPath();

    enum class routingtype {
        SHORT, FAST, ECONOMICAL
    };

    /**
     * do the dijkstra.
     *
     * @param startid   id of start node
     * @param destinationid     id of destination node
     * @param type              short, fast or most economical
     * @param enforceRecalc     true, if solution of previous dijkstra cannot be reused
     */
    void dijkstra (unsigned int startid, unsigned int destinationid, routingtype type, bool enforceRecalc) {

        // we already calculated for this start node and nothing but the destination node has not changed
        if (currentStartId == startid && !enforceRecalc) {
            currentDestId = destinationid;
            return;
        }

        //to remember
        currentStartId = startid;
        currentDestId = destinationid;

        //reset. resize already happened at the end of connect
        //set precedingEdge to maxuint to recognize if precedingEdge was not set!
        assert(nodes.size() < maxuint);

        for (unsigned int i = 0; i < nodes.size(); i++) {
            currentmin[i] = maxlong;
            precedingEdge[i] = maxuint;
            path.clear();
        }

        //zero for start node
        heap.push(DijkstraNode(startid, 0));
        currentmin[startid] = 0;

        /*
         * up to here it's the same. now it differs depending on the mode of routing and transportation
         */
        if (type == routingtype::FAST) {
            while (!heap.empty()) {
                //take min
                double minCost = heap.top().cost;
                unsigned int idOfMin = heap.top().id;

                heap.pop();

                if (minCost > currentmin[idOfMin]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = motorwayOffsets[idOfMin]; i < motorwayOffsets[idOfMin + 1]; i++) {
                    unsigned int succId = motEdges[i].target;
                    double cost = minCost + motEdges[i].traveltime;
                    assert (cost >= 0); //negative costs are not supposed to happen!
                    if (currentmin[succId] > cost) {
                        currentmin[succId] = cost;
                        precedingEdge[succId] = i;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        } else if(type == routingtype::ECONOMICAL) {
            while (!heap.empty()) {
                //take min
                double minCost = heap.top().cost;
                unsigned int idOfMin = heap.top().id;

                heap.pop();

                if (minCost > currentmin[idOfMin]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = motorwayOffsets[idOfMin]; i < motorwayOffsets[idOfMin + 1]; i++) {
                    unsigned int succId = motEdges[i].target;
                    double cost = minCost + motEdges[i].traveltime *
                                            vehicle->clacPges(motEdges[i].maxspeed, motEdges[i].ascent);
                    assert (cost >= 0); //negative costs are not supposed to happen!
                    if (currentmin[succId] > cost) {
                        currentmin[succId] = cost;
                        precedingEdge[succId] = i;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        } else { // SHORT is the default case.
            while (!heap.empty()) {
                //take min
                double minCost = heap.top().cost;
                unsigned int idOfMin = heap.top().id;

                heap.pop();

                if (minCost > currentmin[idOfMin]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = motorwayOffsets[idOfMin]; i < motorwayOffsets[idOfMin + 1]; i++) {
                    unsigned int succId = motEdges[i].target;
                    double cost = minCost + motEdges[i].distance;
                    assert (cost >= 0); //negative costs are not supposed to happen!
                    if (currentmin[succId] > cost) {
                        currentmin[succId] = cost;
                        precedingEdge[succId] = i;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        }
    }
};

#endif //OSMIUM_COUNT_GRAPH_H
