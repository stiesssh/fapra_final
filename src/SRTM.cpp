//
// Created by maumau on 30.08.19.
//

#include "SRTM.h"

#include <cassert>

//for reading
#include <fstream>
//for logging
#include <iostream>

//for name making
#include <sstream>
#include <iomanip>
#include "Utility.h"

void SRTM::read(std::string path) {

    std::ifstream::pos_type size = 2;

    //srtm uses 16 bit integers, thus 2 chars for reading
    char * bytes = new char [size];


    for (int i = 0; i < TILES; i++ ) {
        for (int j = 0; j < TILES; j++) {
            //concatenate file name
            std::stringstream ss;
            ss<< path << std::setfill('0') << "/N" <<std::setw(2)<<(LATMAX-i)<<std::setw(0) <<"E" <<std::setw(3)<<(LONMIN+j)<<std::setw(0) <<".hgt";

            std::ifstream file (ss.str(), std::ios::in|std::ios::binary);

            if (file.is_open())
            {
                //position at beginning of file
                file.seekg (0, std::ios::beg);
                for ( int r = 0; r < ARC ; r++ ) {
                    for ( int c = 0 ; c < ARC; c++ ) {
                        file.read (bytes, size);
                        // reverse byte order cause srmt data is big endian
                        short val = (bytes[0] << 8) | bytes[1];
                        assert (val != -32768);
                        data[r+(i*ARC)][c+(j*ARC)] = val;
                    }
                    //skip right most value of each row
                    file.read (bytes, size);
                }
                file.close();
            } else if ((i <= 1 && j <= 2) || //N56E005 - E007, N55E005 - E007
                    (i == 2 && j <= 1)) { //N54E005, N54E006
                //std::cout << ss.str() << " covers sea only" << std::endl;
                for ( int r = 0; r < ARC ; r++ ) {
                    for ( int c = 0 ; c < ARC; c++ ) {
                        data[r+(i*ARC)][c+(j*ARC)] = 0;
                    }
                }
            } else {
                throw std::runtime_error("could not open file " + ss.str());
            }
        }
    }
}

void SRTM::print(int x, int y, bool partial) {

    if (partial) {
        std::cout << "first row : ";
        for (int c = 0; c < 4; c++) {
            std::cout << data[x * ARC][y * ARC + c] << " ";
        }
        std::cout << " ... ";
        for (int c = ARC-4; c < ARC; c++) {
            std::cout << data[x * ARC][y * ARC + c] << " ";
        }
        std::cout << std::endl;
        std::cout << "last row :";
        for (int c = 0; c < 4; c++) {
            std::cout << data[x * ARC + ARC - 1][y * ARC + c] << " ";
        }
        std::cout << " ... ";
        for (int c = ARC-4; c < ARC; c++) {
            std::cout << data[x * ARC + ARC - 1][y * ARC + c] << " ";
        }

        std::cout << std::endl;
        std::cout << "first column :";
        for (int c = 0; c < 4; c++) {
            std::cout << data[x * ARC + c][y * ARC] << " ";
        }
        std::cout << " ... ";
        for (int c = ARC-4; c < ARC; c++) {
            std::cout << data[x * ARC + c][y * ARC] << " ";
        }
        std::cout << std::endl;
        std::cout << "last column :";
        for (int c = 0; c < 4; c++) {
            std::cout << data[x * ARC + c][y * ARC + ARC - 1] << " ";
        }
        std::cout << " ... ";
        for (int c = ARC-4; c < ARC; c++) {
            std::cout << data[x * ARC + c][y * ARC + ARC - 1] << " ";
        }
        std::cout << std::endl;

    } else {
        std::cout << "first row : ";
        for (int c = 0; c < ARC; c++) {
            std::cout << data[x * ARC][y * ARC + c] << " ";
        }
        std::cout << std::endl;
        std::cout << "last row :";
        for (int c = 0; c < ARC; c++) {
            std::cout << data[x * ARC + ARC - 1][y * ARC + c] << " ";
        }
        std::cout << std::endl;
        std::cout << "first column :";
        for (int c = 0; c < ARC; c++) {
            std::cout << data[x * ARC + c][y * ARC] << " ";
        }
        std::cout << std::endl;
        std::cout << "last column :";
        for (int c = 0; c < ARC; c++) {
            std::cout << data[x * ARC + c][y * ARC + ARC - 1] << " ";
        }
        std::cout << std::endl;
    }
}

void SRTM::print(){
    for ( int r = 0; r < ARC*TILES ; r++ ) {
        for ( int c = 0 ; c < ARC*TILES; c++ ) {
            std::cout << data[r][c];
        }
        std::cout << std::endl;
    }
}

double SRTM::getElevation(double lat, double lon) {
    //normalize to offset to zero & relevant precision of degree
    double nlat = (lat - LATMIN) * ARC;
    double nlon = (lon - LONMIN) * ARC;

    //round down to get index
    int ilat = int(nlat);
    int ilon = int(nlon);

    // exactly on point
    if (0 == Utility::distance(lat, lon, int(lat), int(lon)))
        return data [ilat][ilon];

    // calculate distance to surrounding locations.
    double d1 = 1.0 / Utility::distance(lat, lon, int(lat), int(lon));
    double d2 = 1.0 / Utility::distance(lat, lon, int(lat)+1, int(lon));
    double d3 = 1.0 / Utility::distance(lat, lon, int(lat), int(lon)+1);
    double d4 = 1.0 / Utility::distance(lat, lon, int(lat)+1, int(lon)+1);

    //access data
    short rval1 = data [ilat][ilon];
    short rval2 = data [ilat+1][ilon];
    short rval3 = data [ilat][ilon+1];
    short rval4 = data [ilat+1][ilon+1];

    double rval = (rval1*d1 + rval2*d2 + rval3*d3 + rval4*d4) / (d1 + d2 + d3 + d4);

    return rval;
}