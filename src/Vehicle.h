//
// Created by maumau on 03.09.19.
//

#ifndef FAPRA_VEHICLE_H
#define FAPRA_VEHICLE_H

#include <iostream>
#include <QtCore/QString>

/**
 * a vehicle. hold all vehicle specific information to calculate the fuel usage.
 *
 * formulas taken from:
 *  Stoffregen, Jürgen. Motorradtechnik: Grundlagen und Konzepte von Motor, Antrieb und Fahrwerk. Vieweg+Teubner Verlag, 2012. 8. Auflage
 *
 */
class Vehicle {
private:
    const double R = (1.2041 / 2); // luftdichte/2 an NN bei 20°C in [kg/m^3]
    const double FR = 0.015; // rollkonstante für neuen, festen asphalt in [-]
    const double G = 9.81; //erdbeschleunigung in [m/s^2]

    // energy conversion efficiency (wirkungsgrad)
    // https://de.wikibooks.org/wiki/Motoren_aus_technischer_Sicht/_Vergleich_zwischen_dem_Otto-_und_dem_Dieselmotor
    // https://www.dlr.de/tt/Portaldata/41/Resources/dokumente/ec/Friedrich_Electromobilitaet.pdf
    const double ECF_ELEKTRO = 0.8;
    const double ECF_DIESEL = 0.33;
    const double ECF_BENZIN = 0.25;

    // heating value (heizwert) in [kwh/l]
    // hv [kWh/kg] * density [kg/l] = hv [kWh/l]
    // https://de.wikipedia.org/wiki/Heizwert#Fl%C3%BCssige_Brennstoffe_(bei_25_%C2%B0C)
    const double HV_DIESEL = 11.8 * 0.845;
    const double HV_BENZIN = 11.1 * 0.720;

    // [kWh] = 1000 * 3600 [Ws]
    const double FAKTOR = 1000 * 3600;


public:
    enum class drivetype {
        ELEKTRO, DIESEL, OTTO
    };

private:
    //work this vehicle can do without refuelling in [Ws]
    double totalWork = 0.0;
    //work this vehicle can do until next refuelling in [Ws]
    double actualWork = 0.0;

public:
    Vehicle(QString name, double a, double m, double cw, double tank, drivetype type) : name(name), a(a), m(m), cw(cw), tank(tank),type(type) {
        switch (type) {
            case drivetype::ELEKTRO:
                totalWork = tank * ECF_ELEKTRO * FAKTOR;
                break;
            case drivetype::DIESEL:
                totalWork = tank * HV_DIESEL * ECF_DIESEL * FAKTOR;
                break;
            case drivetype::OTTO:
                totalWork = tank * HV_BENZIN * ECF_BENZIN * FAKTOR;
                break;
            default:
                throw std::runtime_error("unrecognizable dirvetype. cannot handle this.");
        }
        actualWork = totalWork;
    }

    /**
     * calculates the total power required to drive at ankle of sin^{-1}(sina) degree with a velocity of v.
     *
     * @param v the velocitiy in [m/s]
     * @param sina the ankles' sine in [-]
     * @return Pges in [W] = [kg*m^2 / s^3]
     */
    double clacPges(double v, double sina) {
        // luftwiderstand
        double fl = R * v * v * cw * a;
        // rollwiderstand
        double fr = FR * (m+lug) * G;
        // steigungswiderstand
        double fs = (m+lug) * G * sina;
        // beschleunigungswiderstand wird an dieser stelle vernachlässigt.

        double rval = (fl + fr + fs) * v;

        assert (0 < rval);

        return rval;
    }

    /**
     * do some work.
     *
     * @param work the amount of work to be done
     */
    void doWork(double work) {
        actualWork -= work;
    }

    /**
     *
     * @return true, if the tank (or battery) is empty
     */
    bool isEmpty() {
        return actualWork < 0;
    }

    /**
     * Reset the work that can be done to the total work that can be done by this vehicle
     */
    void resetWork() {
        actualWork = totalWork;
    }

    QString name;

    double a; //projected area in [m^2]
    double m; //mass in [kg]
    double cw;  // drag coefficient (Strömungswiderstandskoeffizient) in [-]
    double tank; // amount fuel. in [l] for liquid fuel or in [kwh] for batteries
    double lug = 0.0; //mass of luggage in [kg]

    drivetype type;
};


#endif //FAPRA_VEHICLE_H
