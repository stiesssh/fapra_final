//
// Created by maumau on 08.05.19.
//
#include "Graph.h"
#include "Utility.h"

void Graph::init() {
    connectMotorways();
    std::cout << "graph is connected" << std::endl;
    setAscension();
    std::cout << "ascension is set" << std::endl;
    //things we do not need shall not be there.
    assert(motorwayParsingEdges.capacity() == 0);
    assert(elevation.capacity() == 0);
    assert(osmiumToNode.empty());
    assert(knownNodes.empty());


    //resize dijkstra data structure. this takes a lot of time
    currentmin.resize(nodes.size(), maxuint);
    precedingEdge.resize(nodes.size(), 0);
    std::cout << "finished preparations for dijkstra" << std::endl;

}

void Graph::connectMotorways() {
    for (unsigned long i = motorwayParsingEdges.size(); i > 0; i--) {
        ParsingEdge* e = &motorwayParsingEdges.at(i-1);

        // because parsing edges reference the original osmium nodes
        unsigned int sourceId = osmiumToNode.at(e->source);
        unsigned int targetId = osmiumToNode.at(e->target);

        //calc costs
        double distance = Utility::distance(nodes.at(sourceId).lat, nodes.at(sourceId).lon, nodes.at(targetId).lat,
                          nodes.at(targetId).lon);

        //convert maxSpeed from km/h to m/s to match other units.
        // 1 km/h = 1000 m / 60*60 s = 10 m / 36 s = 5 m / 18 s
        double maxspeed =(e->maxSpeed) * (5. / 18.);

        double traveltime = distance / maxspeed;

        //update edges
        motEdges.emplace_back(Edge(sourceId, targetId, distance, traveltime, maxspeed));
        //remove handled edge from parsing structure
        motorwayParsingEdges.pop_back();
    }
    //won't need this anymore, thus reset
    osmiumToNode.clear();
    osmiumToNode = std::unordered_map<unsigned long, unsigned int>();
    motorwayParsingEdges.clear();
    motorwayParsingEdges = std::vector<ParsingEdge>();

    //sort motorway edges by source node
    struct EdgeComparator {
        bool operator()(Edge& a, Edge& b) const { //& for reference
            return a.source < b.source;
        }
    };
    std::sort(motEdges.begin(), motEdges.end(), EdgeComparator());

    //init offset array
    motorwayOffsets.resize(nodes.size()+1, motEdges.size()-1);

    //set offset
    unsigned long nextSourceId = 0;
    motorwayOffsets.at(0) = 0;
    for (unsigned int i = 0; i < motEdges.size(); i++) {
        unsigned long sourceId = motEdges.at(i).source;
        //set offset and increase
        assert (sourceId < nodes.size() && sourceId < motorwayOffsets.size()); //source id is not too high to set offset
        while (sourceId > nextSourceId) {
            //sourceId has increased, edge is first edge of new block
            //update
            motorwayOffsets.at(++nextSourceId) = i;
        }
    }
}

void Graph::setAscension(){
    for (auto e : motEdges) {
        double sourceElev = elevation[e.source];
        double targetElev = elevation[e.target];

        // negative, if target node is lower than source node
        double geka = targetElev - sourceElev;
        double anka = e.distance;
        double hypo = std::sqrt((anka * anka) + (geka * geka));

        //sinus of ankle of elevation, as this is how its used in further calculations
        //negative if it is a descent.
        e.ascent = geka / hypo;
    }
    elevation = std::vector<double>();
}

size_t Graph::nearestNeighbour(double lat, double lon) {
    size_t nearest = 0;
    double d = std::sqrt((nodes[nearest].lat - lat)*(nodes[nearest].lat - lat)+(nodes[nearest].lon - lon)*(nodes[nearest].lon - lon));
    for (size_t i = 0; i < nodes.size(); i++) {
        Node n = nodes[i];
        double dd =  std::sqrt((n.lat - lat)*(n.lat - lat)+(n.lon - lon)*(n.lon - lon));
        if (dd < d) {
            d = dd;
            nearest = i;
        }
    }
    return nearest;
}

Node Graph::nearestGasstation(double lat, double lon) {
    std::vector<Node> &stations = fuel;
    if (vehicle->type == Vehicle::drivetype::ELEKTRO){
        stations = charging;
    }

    size_t nearest = 0;
    double d = std::sqrt((stations[nearest].lat - lat)*(stations[nearest].lat - lat)+(stations[nearest].lon - lon)*(stations[nearest].lon - lon));
    for (size_t i = 0; i < stations.size(); i++) {
        Node n = stations[i];
        double dd =  std::sqrt((n.lat - lat)*(n.lat - lat)+(n.lon - lon)*(n.lon - lon));
        if (dd < d) {
            d = dd;
            nearest = i;
        }
    }
    return stations.at(nearest);
}


std::vector<unsigned int>& Graph::getPath() {
    if (!path.empty())
        return path;
    //calculate path
    unsigned int dest = currentDestId;

    while (currentStartId != dest){
        if (precedingEdge.at(dest) == maxuint) { //maxuint means that there is no precedingEdge, thus not connected.
            path.clear();
            return path;
        }
        path.emplace_back(precedingEdge.at(dest));

        assert(dest != motEdges[precedingEdge.at(dest)].source); //no loops
        dest = motEdges[precedingEdge.at(dest)].source;
    }
    // because i want to get from start to dest, not the other way round.
    std::reverse(path.begin(), path.end());

    return path;
}