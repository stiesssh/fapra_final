//
// Created by maumau on 08.05.19.
//

#ifndef OSMIUM_COUNT_NODE_H
#define OSMIUM_COUNT_NODE_H
class Node {
public:
    //latitude and longitude in degree
    double lat;
    double lon;

    Node(double _lat, double _lon) : lat(_lat), lon(_lon) {}
};
#endif //OSMIUM_COUNT_NODE_H
