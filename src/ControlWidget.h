//
// Created by maumau on 23.05.19.
//

#ifndef FAPRA_CONTROLWIDGET_H
#define FAPRA_CONTROLWIDGET_H


#include <QtWidgets/QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QWebEngineView>

#include <QListWidget>
#include <QListWidgetItem>

#include "Graph.h"
#include "Node.h"

#include "AddVehicleWidget.h"
#include "PredefinedVehiclesWidget.h"

#include <chrono> //time measurement

class ControlWidget : public QWidget {
Q_OBJECT
private slots:

    /**
     * triggers calculation of route
     */
    void routeme();
    /**
     * triggers update of displayed gas stations
     */
    void updateGas();

    void shortestToggled(bool checked) {
        if (!checked) return;
        type = Graph::routingtype::SHORT;
        anythingToggld = true;
    }

    void fastestToggled(bool checked) {
        if (!checked) return;
        type = Graph::routingtype::FAST;
        anythingToggld = true;
    }

    void economicalToggled(bool checked) {
        if (!checked) return;
        type = Graph::routingtype::ECONOMICAL;
        anythingToggld = true;
    }

    void recommendedToggled(bool checked) {
        if (!checked) return;
        gasDisplayMode = gasDisplayMode::recommended;
    }

    void allOnRouteToggled(bool checked) {
        if (!checked) return;
        gasDisplayMode = gasDisplayMode::allonroute;
    }

    void allAllToggled(bool checked) {
        if (!checked) return;
        gasDisplayMode = gasDisplayMode::allall;
    }

    void noneToggled(bool checked) {
        if (!checked) return;
        gasDisplayMode = gasDisplayMode::none;
    }

    void choosingS() {
        choosing = selectionMode::start;
    }

    void choosingD() {
        choosing = selectionMode::dest;
    }

    void makeVehicle() {
        auto w = new AddVehicleWidget();
        w->setParent(this, Qt::Dialog);
        w->show();
    }

    void chooseVehicle() {
        predef->show();
    }

    void updateLuggageMass(const QString &text) {
        graph->vehicle->lug = text.toDouble();
    }

public slots:
    void getlatlon(const QString &jslat, const QString &jslng);

    void setVehicle(Vehicle* v) {
        label_vname->setText(v->name);
        label_va->setText(QString::number(v->a));
        label_vm->setText(QString::number(v->m));
        label_vcw->setText(QString::number(v->cw));
        label_vtank->setText(QString::number(v->tank));

        if (Vehicle::drivetype::ELEKTRO == v->type) {
            label_vtank_unit->setText("kWh");
        } else {
            label_vtank_unit->setText("l");
        }

        graph->setVehicle(v);
        lineEdit_luggage->setReadOnly(false);
    }

private:
    Graph::routingtype type = Graph::routingtype::SHORT;
    bool anythingToggld = false;

    enum class selectionMode {
        start, dest, none
    };
    selectionMode choosing = selectionMode::none;

    enum class gasDisplayMode {
        recommended, allonroute, allall, none
    };
    gasDisplayMode gasDisplayMode = gasDisplayMode::none;


    double slon = 0.0;
    double slat = 0.0;
    double dlon = 0.0;
    double dlat = 0.0;

    //for current vehicle
    QLabel *label_vname;
    QLabel *label_va;
    QLabel *label_vm;
    QLabel *label_vcw;
    QLabel *label_vtank;
    QLabel *label_vtank_unit;
    QLineEdit *lineEdit_luggage;

    //for display of results
    QLabel *label_dijkstraTime;
    QLabel *label_totalDistance;
    QLabel *label_totalTime;
    QLabel *label_totalWork;

    PredefinedVehiclesWidget * predef;

    QWebEngineView * view;

    Graph *graph;

public:
    explicit ControlWidget(Graph *_graph);

private:
    /**
     * paint polyline for calculated route.
     * polyline is painted as one. its a freaking long string, but its a lot faster than piecewise.
     */
    void paint() {
        resetRoute();

        std::vector<unsigned int> path = graph->getPath();

        if (path.empty()) {
            view->page()->runJavaScript(QStringLiteral("alert( 'start and dest are not connected. try again' );"));
            return;
        }

        QString code = QStringLiteral("var latlngs = [\n");

        for(unsigned int edgeindex : path) {
            Node from = graph->nodes.at(graph->motEdges.at(edgeindex).source);
            QString coord =  QStringLiteral("[");
            coord.append(QString::number(from.lat));
            coord.append( QStringLiteral(", "));
            coord.append(QString::number(from.lon));
            coord.append( QStringLiteral("],\n"));
            code.append(coord);

        }
        Node from = graph->nodes.at(graph->motEdges.at(path.at(path.size()-1)).target);
        code.append(QStringLiteral("["));
        code.append(QString::number(from.lat));
        code.append(QStringLiteral(", "));
        code.append(QString::number(from.lon));
        code.append(QStringLiteral("]\n"));

        code.append(QStringLiteral("];\n"
                                   "var polyline = L.polyline(latlngs, {color: 'red'}).addTo(mymap);\n"
                                   "// zoom the map to the polyline\n"
                                   "mymap.fitBounds(polyline.getBounds());"));
        view->page()->runJavaScript(code);

        updateResultDisplay();

        if (gasDisplayMode::allonroute == gasDisplayMode || gasDisplayMode::recommended == gasDisplayMode) {
            displayGas();
        }

    }

    /**
     * display gas stations on route, either all or only recommended
     */
    void displayGas();

    /**
     * display all gas stations
     */
    void displayAllGas();

    /**
     * remove painted route and all route specific gas stations, that is if all gas stations are on display they remain on display.
     */
    void resetRoute();

    /**
     * recalculate total time, distance and work of current route.
     * does not affect dijkstra time, as that label is handled directly in routeme.
     */
    void updateResultDisplay ();
};

#endif //FAPRA_CONTROLWIDGET_H
