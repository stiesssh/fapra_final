# ! /bin/sh
# check correct usage
if [$1 = ""] || [$2 = ""]; then
    echo "usage : get_srt.sh [nasa-username] [nasa-password]";
    exit;
fi 

# clean folder
rm *.hgt.zip
rm *.hgt

# download and unzip files
for i in $(seq 46 56); do 
    for j in $(seq  5 9); do  
        wget --user $1 --password $2 http://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/$(echo "N"$i"E00"$j).SRTMGL1.hgt.zip; 
        unzip $(echo "N"$i"E00"$j).SRTMGL1.hgt.zip;
    done;
    for j in $(seq  10 15); do  
        wget --user $1 --password $2 http://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/$(echo "N"$i"E0"$j).SRTMGL1.hgt.zip; 
        unzip $(echo "N"$i"E0"$j).SRTMGL1.hgt.zip;
    done;
done;

# clean folder
rm *.hgt.zip
