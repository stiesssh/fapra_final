# Fachpraktikum Algorithms on OpenStreetMap data

## Requirements
This project requires the following libraries. 
It was tested with the versions mentioned below. 
Other versions might work as well.

* cmake version 3.10.2

* gcc 7.4.0

* Qt5.12.3

* libosmium2.15.1

Install libosmium like this:

    sudo add-apt-repository ppa:osmadmins/ppa
    sudo apt-get update
    sudo apt-get install libosmium2-dev

Or follow the instructions described here:

[https://github.com/osmcode/libosmium](https://github.com/osmcode/libosmium)

[https://osmcode.org/libosmium/manual.html](https://osmcode.org/libosmium/manual.html)

How to install Qt5.12.3:

    wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
    chmod +x qt-unified-linux-x64-online.run
    ./qt-unified-linux-x64-online.run

## Required Data
### OSM Data
This project uses OSM Data in .pdf format to get information on the German road network. 

### SRTM Data
This project uses data from NASA's Shuttle Radar Topography Mission (SRTM) to get information on the ascent and descent of German roads.
The project requires the tiles from Latitude N46° to N56° and from Longitude E5° to E15° to cover all of Germany.

The script get_srtm.sh in srtm/ can be used to download the relevant tiles.

## Building

    cd <path to project>
    mkdir build
    cd build
    cmake -Bbuild -DCMAKE_PREFIX_PATH=<path to qt>/Qt/5.12.3/gcc_64/lib/cmake/Qt5 ../src
    make

## Running

```
./fapra <path to pbf> [<path to srtm folder>]
```
The path to the srtm folder is optional. If no path is specified ../srtm is used as default.

## Usage
### Basic Routing
* Press the button **Choose start on map** and click on the map to set the start point.
* Press the button **Choose dest on map** and click on the map to set the destination point.
* Press the button **Route me** to calculate and display the route from the start to the destination point.

The chosen points are marked on the map and a their coordinates are displayed next to their markers. 
The calculated route is displayed as a thick red line. 
If start and destination points are not connected a popup appears.

### Routing Options
* Choose the mode of routing. **shortest route** calculates the route with the least distance, **fastest route** calculates the route with the least travel time and **most economical route** calculates the route, that needs the least energy.
* Apply the new mode by recalculating the route, that is press **Route me** again.

### Vehicle Options
* Press the button **Choose Vehicle** to choose one of the predefined vehicles as the current one.
* Press the button **Make new Vehicle** to make a new vehicle. This option requires floating point values as input and the input is not sanitized.
* Enter floating point values as **Luggage** to simulate additional weight in the vehicle, e.g. the driver.

### Gas Station Options
* Choose which gas stations to display.
    * **display recommended gas stations** displays gas stations near the position where your gas tank switches to reserve. 
    * **display all gas stations on route** displays gas stations on the calculated route.The gas stations displayed by these two modes depend on the current vehicle. If it is a electrical vehicles, charging stations are displayed (in orange). If it runs on liquid fuel, normal gas stations are displayed (in blue).
    * **display all gas stations** displays all gas stations and charging stations all over Germany,
    * **display no gas stations** does not display any gas stations at all. 

* Press the button **Update Gas Display** to apply the new mode to the currently displayed route. If no route is on display, no gas stations are displayed either.
* The current mode of display applies to all following recalculations of the route.

## Problems that might (did) happen
* Failed to find "GL/gl.h" in "/usr/include/libdrm".

   apt install mesa-common-dev

   or anything else, that contains the missing file.

* qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found. [...]

   apt install libqt5\* 

   one of those libraries fixes the problem, i just do not know which one.

## Author

Sarah Sophie Stieß (2963899)(st115875@stud.uni-stuttgart.de)


